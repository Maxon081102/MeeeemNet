import random
from types import new_class
import pandas as pd
import numpy as np

from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer(language='english')

class_ = {"Music":0, 
          "AppStore":1, 
          "Anime":2,
          "Man of the Year":3, 
          "Article":4, 
          "Steam":5, 
          "film":6, 
          "Hero":7, 
          "Company":8,
          "Book":9,
          "BoardGame":10
          }
class Document:
    def __init__(self, title, text):
        # можете здесь какие-нибудь свои поля подобавлять
        self.title = title
        self.text = text
    
    def format(self, query):
        # возвращает пару тайтл-текст, отформатированную под запрос
        #return [self.title, self.text + ' ...']
        return [self.title, self.text]

index = []

def add_data(name, type, name_1, name_2=False, dropna=False):
    df = pd.read_csv(name)
    df = df.drop_duplicates()
    if dropna:
       df = df[[name_1, name_2]].dropna() 
    libr = df[[name_1]].values
    if name_2:
        libr2 = df[["track_name"]].values
        for i in range(len(libr)):
            index.append(Document(libr2[i][0] + " - " + libr[i][0], type)) 
    else:
        for i in range(len(libr)):
            index.append(Document(libr[i][0], type))  


def build_index():
    # считывает сырые данные и строит индекс
    #MUSIC
    df = pd.read_csv("data/SPotifyFeatures.csv")
    df = df[["artist_name","track_name"]]
    df = df.drop_duplicates()
    libr = df[["artist_name"]].values
    songs = df[["track_name"]].values
    for i in range(len(libr)):
        index.append(Document(songs[i][0] + " - " + libr[i][0], "Music"))



    #appstoreGames
    df = pd.read_csv("data/appstore_games.csv")
    df = df[["Name","Developer"]]
    df = df.drop_duplicates()
    name_appstore = df[["Name"]].values
    dev = df[["Developer"]].values
    for i in range(len(name_appstore)):
        index.append(Document(name_appstore[i][0] + " - " + dev[i][0], "AppStore"))
    


    #anime
    df = pd.read_csv("data/anime.csv")
    df = df[["name"]]
    df = df.drop_duplicates()
    name_anime = df[["name"]].values
    for i in range(len(name_anime)):
        index.append(Document(name_anime[i][0], "Anime"))
    

    #Man of the year
    df = pd.read_csv("data/archive.csv")
    df = df[["Name","Category"]].dropna()
    df = df.drop_duplicates()
    name_of_man = df[["Name"]].values
    cat = df[["Category"]].values
    for i in range(len(name_of_man)):
        index.append(Document(name_of_man[i][0] + " - " + cat[i][0], "Man of the Year"))
    

    #articles
    df = pd.read_csv("data/articles.csv")
    df = df[["title"]]
    df = df.drop_duplicates()
    title = df[["title"]].values
    for i in range(len(title)):
        index.append(Document(title[i][0], "Article"))



    #steamGameas
    df = pd.read_csv("data/steam.csv")
    df = df[["name","developer"]]
    df = df.drop_duplicates()
    name_game = df[["name"]].values
    dev = df[["developer"]].values
    for i in range(len(name_game)):
        index.append(Document(name_game[i][0] + " - " + dev[i][0], "Steam"))


    #imdb
    df = pd.read_csv("data/imdb.csv")
    df = df[["Name"]]
    df = df.drop_duplicates()
    title = df[["Name"]].values
    for i in range(len(title)):
        index.append(Document(title[i][0], "film"))
    


    #heroes
    df = pd.read_csv("data/heroes_information.csv")
    df = df[["name","Publisher"]].dropna()
    df = df.drop_duplicates()
    name_hero = df[["name"]].values
    pub = df[["Publisher"]].values
    for i in range(len(name_hero)):
        index.append(Document(name_hero[i][0] + " - " + pub[i][0], "Hero"))



    #companies
    df = pd.read_csv("data/fortune 1000 companies in dec2021.csv")
    df = df[["Name"]]
    df = df.drop_duplicates()
    title = df[["Name"]].values
    for i in range(len(title)):
        index.append(Document(title[i][0], "Company"))



    #books
    df = pd.read_csv("data/books.csv")
    df = df[["title","authors"]].dropna()
    df = df.drop_duplicates()
    title= df[["title"]].values
    aut = df[["authors"]].values
    for i in range(len(title)):
        index.append(Document(title[i][0] + " - " + aut[i][0], "Book")) 
    


    #bgg
    df = pd.read_csv("data/bgg_dataset.csv", sep = ";")
    df = df[["Name"]]
    df = df.drop_duplicates()
    name_bgg = df[["Name"]].values
    for i in range(len(name_bgg)):
        index.append(Document(name_bgg[i][0], "BoardGame")) 

def pearson_similarity(x: np.array, y: np.array) -> float:
    """
    Calculate a Pearson correlation coefficient given 1-D data arrays x and y
    Args:
        x, y: two points in n-space
    Returns:
        Pearson correlation between x and y
    """
    n = x.shape[0]
    zn = (np.sqrt((x**2).sum() - n * (x.mean()**2)) * np.sqrt((y**2).sum() - n * (y.mean()**2)))
    if zn < 0.0003:
        return np.nan
    return ((x * y).sum() - n * x.mean() * y.mean())/zn



def score(query, document):
    # возвращает какой-то скор для пары запрос-документ
    # больше -- релевантнее
    if query == "":
        return 1
    title = ' '.join([stemmer.stem(word) for word in document.text.split()])
    query = ' '.join([stemmer.stem(word) for word in query.split()])
    document_ = ' '.join([stemmer.stem(word) for word in document.title.split()])
    idf_vectorizer = TfidfVectorizer()
    idf_vectorizer.fit([query, document_, title])
    X = idf_vectorizer.transform([document_])
    Y = idf_vectorizer.transform([query])
    return pearson_similarity(np.array(X.todense()), np.array(Y.todense()))

def intersection(first_c, second_c):
    c_for_first_and_second = []
    i, j = 0, 0
    while i < len(first_c) and j < len(second_c):
        if first_c[i] == second_c[j]:
            c_for_first_and_second.append(first_c[i])
            i += 1
            j += 1
        
        elif first_c[i] < second_c[j]:
            i +=1
        
        elif second_c[j] < first_c[i]:
            j += 1
    return c_for_first_and_second

def retrieve(query):
    # возвращает начальный список релевантных документов
    # (желательно, не бесконечный)
    if query == "":
        return [doc for doc in index][:50]
    words_in_query = query.lower().split()
    candidates_for_word = [[] for i in range(len(words_in_query))]
    for i in range(len(words_in_query)):
        for doc, j in zip(index, range(len(index))):
            if words_in_query[i] in doc.title.lower() or words_in_query[i] in doc.text.lower():
                candidates_for_word[i].append(j)
    candidates = [elem for elem in candidates_for_word[0]]
    for i in range(1 ,len(words_in_query)):
        candidates = intersection(candidates, candidates_for_word[i])
    candidates = [index[elem] for elem in candidates]
    if len(candidates) > 500:
        return candidates[:500]
    else:
        return candidates

def second_processing(query, candidates):
    if query == "":
        return candidates
    
    if len(candidates) > 100:
        new_candidates = []
        old = []
        words_in_query = query.lower().split()
        for i in range(len(words_in_query)):
            for j in range(i + 1, len(words_in_query)):
                ph1 = words_in_query[i] + words_in_query[j]
                ph2 = words_in_query[j] + words_in_query[i] 
                for k in range(len(candidates)):
                    if ph1 in candidates[k].text.lower() or ph1 in candidates[k].title.lower() or ph2 in candidates[k].title.lower() or ph2 in candidates[k].text.lower():
                        new_candidates.append(candidates[k])
                    else:
                        old.append(candidates[k])
        if len(new_candidates) < 50:
            cl_c = []
            for i in range(len(old)):
                cl_c.append([score(query, old[i]), old[i]])
            cl_c.sort(key = lambda cl_c : cl_c[0], reverse=True)
            for i in range(len(cl_c)//5):
                new_candidates.append(cl_c[i][1])
        return new_candidates
    else:
        return candidates
    
def apk(actual: np.array, predicted: np.array, k: int = 10) -> float:
    """
    Compute the average precision at k
    Args:
        actual: a list of elements that are to be predicted (order doesn't matter)
        predicted: a list of predicted elements (order does matter)
        k: the maximum number of predicted elements
    Returns:
        The average precision at k over the input lists
    """
    N = 0
    rel = 0
    for i in range(1, k + 1):
        if predicted[i - 1] in actual:
            rel = 1
        N += len(set(actual) & set(predicted[ :i])) * rel/ i
        rel = 0
    return N/k

def check(query, results):
    if query.lower() == "beatles":
        res = []
        answer = [0, 0, 0, 0, 0, 0, 0, 0, 0,0]
        for i in range(len(answer)):
            res.append(class_[results[i][0].text])
        print(f"apk result for {query} = {apk(np.array(answer), np.array(res))}")
    
    if query.lower() == "batman dc":
        answer = ["Batman - DC Comics", "Batman II - DC Comics", "DC Comics Deck-Building Game: Rivals - Batman vs The Joker"]
        res = []
        for i in range(len(answer)):
            res.append(results[i][0].title)
        print(f"apk result for {query} = {apk(np.array(answer), np.array(res), 3)}")

    if query.lower() == "game of  thrones":
        res = []
        answer = [6, 6, 0, 0, 0, 0, 5, 10, 10,10]
        for i in range(len(answer)):
            res.append(class_[results[i][0].text])
        print(f"apk result for {query} = {apk(np.array(answer), np.array(res))}") 
    
    if query.lower() == "deep learning":
        answer = ["Video of a neural network learning – Deep Learning 101 – Medium", 
                  "How to learn Deep Learning in 6 months – Towards Data Science", 
                  "Algorithms of the Mind – Deep Learning 101 – Medium",
                  "How to do Semantic Segmentation using Deep learning",
                  "Must know Information Theory concepts in Deep Learning (AI)"]
        res = []
        for i in range(len(answer)):
            res.append(results[i][0].title)
        print(f"apk result for {query} = {apk(np.array(answer), np.array(res), 5)}")
    
    if query.lower() == "gatsby":
        res = []
        answer = [9, 6, 9, 9, 9]
        for i in range(len(answer)):
            res.append(class_[results[i][0].text])
        print(f"apk result for {query} = {apk(np.array(answer), np.array(res), 5)}")

    if query.lower() == "naruto":
        answer = ["Naruto", 
                  "Naruto: Shippuuden", 
                  "Naruto",
                  "Naruto Uzumaki - Shueisha",
                  "Boruto: Naruto the Movie",
                  "Sign - Naruto Opening Mix - FLOW",
                  "NARUTO: Ultimate Ninja STORM - CyberConnect2 Co. Ltd."]
        res = []
        for i in range(len(answer)):
            res.append(results[i][0].title)
        print(f"apk result for {query} = {apk(np.array(answer), np.array(res), 7)}")  
    
    if query.lower() == "bad boys":
        res = []
        answer = [6, 6, 6, 0, 0, 0, 0, 0, 0,0]
        for i in range(len(answer)):
            res.append(class_[results[i][0].text])
        print(f"apk result for {query} = {apk(np.array(answer), np.array(res))}")

    if query.lower() == "counter-strike":
        answer = ["Counter-Strike: Global Offensive - Valve;Hidden Path Entertainment", 
                  "Counter-Strike - Valve", 
                  "Counter-Strike: Source - Valve",
                  "Counter-Strike: Condition Zero - Valve",
                  "Counter-Strike Nexon: Zombies - Valve Corporation, Nexon Korea Corporation"]
        res = []
        for i in range(len(answer)):
            res.append(results[i][0].title)
        print(f"apk result for {query} = {apk(np.array(answer), np.array(res), 5)}")


    


        